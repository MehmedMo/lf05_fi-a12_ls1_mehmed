import java.util.ArrayList;

public class Raumschiff {
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	public Raumschiff() {
		
	}

	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahlNeu) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahlNeu;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzentNeu) {
		this.energieversorgungInProzent = energieversorgungInProzentNeu;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzentNeu) {
		this.schildeInProzent = schildeInProzentNeu;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzentNeu) {
		this.huelleInProzent = huelleInProzentNeu;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzentNeu) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzentNeu;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahlNeu) {
		this.androidenAnzahl = androidenAnzahlNeu;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsnameNeu) {
		this.schiffsname = schiffsnameNeu;
	}
	
	public void addLadung(Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}

	@Override
	public String toString() {
		return "Raumschiff [photonentorpedoAnzahl = " + photonentorpedoAnzahl + ",\n energieversorgungInProzent = "
				+ energieversorgungInProzent + ",\n schildeInProzent = " + schildeInProzent + ",\n huelleInProzent = "
				+ huelleInProzent + ", \n lebenserhaltungssystemeInProzent = " + lebenserhaltungssystemeInProzent
				+ ", \n androidenAnzahl = " + androidenAnzahl + ", \n schiffsname = " + schiffsname + ", \n broadcastKommunikator = "
				+ broadcastKommunikator + ", \n ladungsverzeichnis = " + ladungsverzeichnis + "]";
	}

	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

//	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
//		this.ladungsverzeichnis = ladungsverzeichnis;
//	}
	
	public String zustandRaumschiff() {
		return "Raumschiffzustand: \n energieVersorgungInProzent = " + energieversorgungInProzent + ", \n schildeInProzent = " + schildeInProzent 
				+ ", \n huelleInProzent = " + huelleInProzent + ", \n lebenserhaltungssystemeInProzent = " + lebenserhaltungssystemeInProzent;
	}
	
	public void zustandRaumschiff2() {
		System.out.println("Raumschiffzustand der " + schiffsname + ": \n energieVersorgungInProzent = " + energieversorgungInProzent + ", \n schildeInProzent = " + schildeInProzent 
				+ ", \n huelleInProzent = " + huelleInProzent + ", \n lebenserhaltungssystemeInProzent = " + lebenserhaltungssystemeInProzent);
	}
	
	public void ladungsverzeichnisAusgeben() {
		for (int i = 0; i < ladungsverzeichnis.size(); i++) {
			System.out.println(ladungsverzeichnis.get(i));
			;
		}
	}


//    Gibt es keine Ladung Photonentorpedo auf dem Schiff, wird als Nachricht Keine Photonentorpedos gefunden! 
//    in der Konsole ausgegeben und die Nachricht an alle -=*Click*=- ausgegeben.
//    Ist die Anzahl der einzusetzenden Photonentorpedos größer als die Menge der tatsächlich Vorhandenen, 
//    so werden alle vorhandenen Photonentorpedos eingesetzt.
//    Ansonsten wird die Ladungsmenge Photonentorpedo über die Setter Methode vermindert und die Anzahl 
//    der Photonentorpedo im Raumschiff erhöht.
//    Konnten Photonentorpedos eingesetzt werden, so wird die Meldung [X] Photonentorpedo(s) eingesetzt auf 
//    der Konsole ausgegeben. [X] durch die Anzahl ersetzen.

	public void photonentorpedoSchiessen(Raumschiff r) {
		if(photonentorpedoAnzahl != 0) {
			nachrichtAnAlle("Photonentorpedo abgeschossen");
			photonentorpedoAnzahl--;
			treffer(r);
		}else {
			System.out.println("Keine Photonentorpedos gefunden!");
			nachrichtAnAlle("-=*Click*=-");
		}
	}
	
	public void phaserKanoneSchiessen(Raumschiff r) {
		if( energieversorgungInProzent >= 50) {
			nachrichtAnAlle("Phaserkanone abgeschossen");
			energieversorgungInProzent = energieversorgungInProzent - 50;
			treffer(r);
		}else {
			nachrichtAnAlle("-=*Click*=-");
		}
	}
	
	public void nachrichtAnAlle(String nachricht) {
		System.out.println(nachricht);
		broadcastKommunikator.add(nachricht);
	}
	
	public void treffer(Raumschiff r) {
		System.out.println(r.schiffsname + " wurde getroffen!");
		r.schildeInProzent = (int) (r.schildeInProzent * 0.5);
		if (r.schildeInProzent == 0 ) {
			r.energieversorgungInProzent = (int) (r.energieversorgungInProzent * 0.5);
			r.huelleInProzent = (int) (r.huelleInProzent * 0.5);
		}
		if(r.huelleInProzent == 0) {
			r.lebenserhaltungssystemeInProzent = 0;
			r.nachrichtAnAlle("Die Lebenserhaltungssystem wurden vernichtet.");
		}
	}
	

//    Die Schilde des getroffenen Raumschiffs werden um 50% geschwächt.
//    Sollte anschließend die Schilde vollständig zerstört worden sein, 
//    so wird der Zustand der Hülle und der Energieversorgung jeweils um 50% abgebaut.
//    Sollte danach der Zustand der Hülle auf 0% absinken, so sind die Lebenserhaltungssysteme 
//    vollständig zerstört und es wird eine Nachricht an Alle ausgegeben, dass die Lebenserhaltungssysteme vernichtet worden sind.

	
	public static ArrayList<String> eintraegeLogbuchZurueckgeben() {
		return broadcastKommunikator;
	}
	
	public void photonentorpedosLaden(int anzahlTorpedos) {
		this.photonentorpedoAnzahl += anzahlTorpedos;
	}
	
}
