import java.util.ArrayList;

public class Raumschifftest {

	public static void main(String[] args) {
		
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
//		System.out.println(klingonen);
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
//		System.out.println(romulaner);
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni'Var");
//		System.out.println(vulkanier);
		
		Ladung l1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung l2 = new Ladung("Bat'leth klingonen Schwert", 200);
		klingonen.addLadung(l1);
		klingonen.addLadung(l2);
		
		Ladung l3 = new Ladung("Borg-Schrott", 5);
		Ladung l4 = new Ladung("Rote Materie", 2);
		Ladung l5 = new Ladung("Plasma-Waffe", 50);
		romulaner.addLadung(l3);		
		romulaner.addLadung(l4);
		romulaner.addLadung(l5);
		
		Ladung l6 = new Ladung("Forschungssonde", 35);
		Ladung l7 = new Ladung("Photonentorpedo", 3);
		vulkanier.addLadung(l6);
		vulkanier.addLadung(l7);
		
		
		
		/////////////////////////  Ausgaben //////////////////////////
		
//		System.out.println(klingonen.getLadungsverzeichnis() + "\n");
//		System.out.println(romulaner.getLadungsverzeichnis() + "\n");
//		System.out.println(vulkanier.getLadungsverzeichnis() + "\n");
//		
//		System.out.println(klingonen.zustandRaumschiff());
//		
//		klingonen.zustandRaumschiff2();
//		
//		klingonen.ladungsverzeichnisAusgeben();
//		
//		klingonen.photonentorpedoSchiessen(romulaner);
//		klingonen.photonentorpedoSchiessen(romulaner);
//		klingonen.photonentorpedoSchiessen(romulaner);
//
//
//		klingonen.phaserKanoneSchiessen(vulkanier);
//		klingonen.phaserKanoneSchiessen(vulkanier);
//		klingonen.phaserKanoneSchiessen(vulkanier);
		
		klingonen.photonentorpedoSchiessen(romulaner);
		romulaner.phaserKanoneSchiessen(klingonen);
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
		klingonen.zustandRaumschiff2();
		klingonen.ladungsverzeichnisAusgeben();
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.photonentorpedoSchiessen(romulaner);
		
		klingonen.zustandRaumschiff2();
		klingonen.ladungsverzeichnisAusgeben();
		romulaner.zustandRaumschiff2();
		romulaner.ladungsverzeichnisAusgeben();
		vulkanier.zustandRaumschiff2();
		vulkanier.ladungsverzeichnisAusgeben();
		System.out.println("Einträge des Logbuchs: " + Raumschiff.eintraegeLogbuchZurueckgeben());		
	}

}
