import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen

//		System.out.println("Geben Sie den Namen des Aritekls an: ");
		String artikel = liesString("Geben Sie den Namen des Aritekls an: ");
		
//		System.out.println("Geben Sie die Anzahl ein:");
		int anzahl = liesInt("Geben Sie die Anzahl ein: ");

//		System.out.println("Geben Sie den Nettopreis ein:");
		double nettopreis = liesInt("Geben Sie den Nettopreis ein: ");

//		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		
		double mwst = liesInt("Geben Sie den Mehrwertsteuersatz in Prozent ein:");

		// Verarbeiten
		double nettogesamtpreis2 = berechneGesamtnettopreis(anzahl,nettopreis);
//		double nettogesamtpreis = anzahl * nettopreis;
		double bruttogesamtpreis2 = berechneGesamtbruttopreis(nettogesamtpreis2, mwst);
//		double bruttogesamtpreis = nettogesamtpreis2 * (1 + mwst / 100);

		// Ausgeben

//		System.out.println("\tRechnung");
//		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis2);
//		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis2, mwst, "%");
		rechungausgeben(artikel, anzahl, nettogesamtpreis2, bruttogesamtpreis2, mwst);

	}

	public static String liesString(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.print(text);
		String wert = myScanner.next();
		return wert;
	}
	
	public static int liesInt(String text){
		Scanner myScanner = new Scanner(System.in);
		System.out.print(text);
		int wert = myScanner.nextInt();
		return wert;
	}
	
	public static double liesDouble(String text){
		Scanner myScanner = new Scanner(System.in);
		System.out.print(text);
		double wert = myScanner.nextInt();
		return wert;
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		double nettogesamtpreis = anzahl * nettopreis;
		return nettogesamtpreis;	
	}
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		double gesamt_brutto_preis = nettogesamtpreis * (1 + mwst / 100);
		return gesamt_brutto_preis;
	}
	
	public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis2, double bruttogesamtpreis2, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis2);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis2, mwst, "%");
	}
}
