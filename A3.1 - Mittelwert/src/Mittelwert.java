import java.util.Scanner;

public class Mittelwert {

	public static double liesDouble(String eingabeText) {
		Scanner myScanner = new Scanner(System.in);
		System.out.print(eingabeText);
		double wert = myScanner.nextDouble();
		return wert;
	}
	
   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte für x und y festlegen:
      // ===========================
      double x = liesDouble("Gib eine Zahl ein: ");
      double y = liesDouble("Gib eine weitere Zahl ein: ");
      double m;
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      m = (x + y) / 2.0;
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
      
      
      double xx = berechneMittelwert(10,20);
      System.out.println(xx);
   }
   
 public static double berechneMittelwert(double x, double y){ 
	 return (x + y) / 2; 
 }
 
}
