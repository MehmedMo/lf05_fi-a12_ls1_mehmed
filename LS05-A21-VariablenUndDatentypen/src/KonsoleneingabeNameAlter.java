import java.util.Scanner;

public class KonsoleneingabeNameAlter {

	public static void main(String[] args) {
	    // Neues Scanner-Objekt myScanner wird erstellt     
	    Scanner myScanner = new Scanner(System.in);  
	     
	    System.out.print("Hallo! Wie lautet dein Name? ");    
	     
	    String userName = myScanner.next();  
	     
	    System.out.print("Das ist ein schöner Name, wie alt bist du " + userName + "? "); 
	     
	    int userAge = myScanner.nextInt();  
	     
	    System.out.print("Super, du heißt also " + userName + " und du bist " + userAge + " Jahre alt.");
	 
	    myScanner.close(); 
	}

}
