
public class AusgabeAufgabe2 {

	public static void main(String[] args) {
		String star = "*************";
			
		System.out.printf( "%7.1s\n", star );
		System.out.printf( "%8.3s\n", star );
		System.out.printf( "%9.5s\n", star );
		System.out.printf( "%10.7s\n", star );
		System.out.printf( "%11.9s\n", star );
		System.out.printf( "%12.11s\n", star );
		System.out.printf( "%13.13s\n", star );
		System.out.printf( "%8.3s\n", star );
		System.out.printf( "%8.3s\n", star );

		
		System.out.printf("%8.2s\n", star);
		System.out.printf("%1.1s\n", star);
	}

}