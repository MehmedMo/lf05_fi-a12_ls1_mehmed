
public class UebungZweiA2o {

	public static void main(String[] args) {
		
		
	
		
		System.out.printf("Name: %-10s|Age: %-10d|Gewicht: %-10.2f%n", "Mehmed", 23, 70.2425);
		
		System.out.printf("%-5s=%-19s=%4d%n", "0!", "", 1);
		System.out.printf("%-5s=%-19s=%4d%n", "1!", " 1", 1);
		System.out.printf("%-5s=%-19s=%4d%n", "2!", " 1 * 2", 2);
		System.out.printf("%-5s=%-19s=%4d%n", "3!", " 1 * 2 * 3", 6);
		System.out.printf("%-5s=%-19s=%4d%n", "4!", " 1 * 2 * 3 * 4", 24);
		System.out.printf("%-5s=%-19s=%4d%n", "5!", " 1 * 2 * 3 * 4 * 5", 120);
		
		System.out.printf("%-12s|%10s%n", "Fahreneit", "Celsius");
		System.out.println("------------------------");
		System.out.printf("%-12d|%10.2f%n", -20, -28.89);
		System.out.printf("%-12d|%10.2f%n", -10, -23.33);
		System.out.printf("%-12d|%10.2f%n", 0, -17.78);
		System.out.printf("%-12d|%10.2f%n", 20, -6.67);
		System.out.printf("%-12d|%10.2f%n", 30, -1.11);
	}

}