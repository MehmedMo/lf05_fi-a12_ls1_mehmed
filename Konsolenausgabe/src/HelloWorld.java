
public class HelloWorld {

	public static void main(String[] args) {
		double num01 = 22.4234234;
		double num02 = 111.2222;
		double num03 = 4.0;
		double num04 = 1000000.551;
		double num05 = 97.34;
		
		System.out.printf("%.2f\n", num01);
		System.out.printf("%.2f\n", num02);
		System.out.printf("%.2f\n", num03);
		System.out.printf("%.2f\n", num04);
		System.out.printf("%.2f\n", num05);
	}

}