﻿import java.util.Scanner;

class Fahrkartenautomat
{
	public static Scanner tastatur = new Scanner(System.in);
	

	
    public static void main(String[] args)
    {
       
       while(true) {
       double zuZahlenderBetrag = fahrkartenbestellungErfassen();
    	
       double rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

       fahrkartenAusgeben();

       rueckgeldAusgeben(rückgabebetrag);
//       System.out.println("Um ein neuen Ticket zu bestellen klicken Sie eine beliebige Taste");
//       tastatur.();
       }

    }
    
    static double fahrkartenbestellungErfassen() {
        double zuZahlenderBetrag; 
        int anzahlDerTickets;        
        double[] ticketPreise = {	2.90, 
        							3.30, 
        							3.60,
        							1.90,
        							8.60,
        							9.00,
        							9.60,
        							23.50,
        							24.30,
        							24.90};
        String[] tickets = {"Einzelfahrschein Berlin AB", 
        					"Einzelfahrschein Berlin BC", 
        					"Einzelfahrschein Berlin ABC",
        					"Kurzstrecke",
        					"Tageskarte Berlin AB",
        					"Tageskarte Berlin BC",
        					"Tageskarte Berlin ABC",
        					"Kleingruppen-Tageskarte Berlin AB",
        					"Kleingruppen-Tageskarte Berlin BC",
        					"Kleingruppen-Tageskarte Berlin ABC"};

        
        System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
//        System.out.println("  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
//        System.out.println("  Tageskarte Regeltarif AB [8,60 EUR] (2)");
//        System.out.println("  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
        
        for (int i = 0; i < tickets.length; i++) {
        	  System.out.printf("  " + tickets[i] + " [%.2f EUR] " + "(" + (i+1) + ")\n", ticketPreise[i]);
        	}

//        System.out.println("  " + tickets[0] + " [2,90 EUR] (1)");
//        System.out.println("  " + tickets[1] + " [3,30 EUR] (2)");
//        System.out.println("  " + tickets[2] + " [3,60 EUR] (3)");
        
        int ticketAuswahl = tastatur.nextInt() - 1;
        while(ticketAuswahl >= tickets.length || ticketAuswahl <= -1 ) {
            System.out.println("Fehler Falsche Angabe");
            ticketAuswahl = tastatur.nextInt() - 1;
        }
        
        zuZahlenderBetrag = ticketPreise[ticketAuswahl];

        System.out.print("Anzahl der Tickets: ");
        anzahlDerTickets = tastatur.nextInt();
        
        if(anzahlDerTickets > 10 || anzahlDerTickets < 1) {
            System.out.println("Die Ticketanzahl muss zwischen 1 und 10 sein. Aufgrund der inakzeptablen Eingabe wurde die Anzahl der Tickets auf 1 gesetzt.");
        	anzahlDerTickets = 1;
            zuZahlenderBetrag = zuZahlenderBetrag * anzahlDerTickets;
            return zuZahlenderBetrag;
        } else {
            zuZahlenderBetrag = zuZahlenderBetrag * anzahlDerTickets;
            return zuZahlenderBetrag;
        }

    }

    static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        double eingeworfeneMünze;
        double rückgabebetrag;
        double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen: %.2f Euro%n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
     	   
     	   while(eingeworfeneMünze > 2 || eingeworfeneMünze < 0.05) {
     		   System.out.print("Bitte geben Sie passende Geldbeträge ein: ");
         	   eingeworfeneMünze = tastatur.nextDouble();
     	   }
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        
        
        rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        return rückgabebetrag;
    }

    static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n\n");
    }

    static void rueckgeldAusgeben(double rückgabebetrag) {
    	if(rückgabebetrag > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n", rückgabebetrag );
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
            	muenzeAusgeben(2, "Euro");
            	System.out.printf("   ****   %n"
                        + "  * %2s *  %n"
                        + " * %s * %n"
                        + "  *    *%n"
                        + "   ****   %n", 2, "Euro");

 	          	rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
            	muenzeAusgeben(1, "Euro");
 	          	rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
            	muenzeAusgeben(50, "Cent");
            	rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
            	muenzeAusgeben(20, "Cent");
  	          	rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
            	muenzeAusgeben(10, "Cent");
            	rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
            	muenzeAusgeben(5, "Cent");
  	          	rückgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.\n"+
        				 "========================================\n");
      
        
//        tastatur.close(); 
        
    }
    
    static void warte(int millisekunde) {
    	try {
 			Thread.sleep(millisekunde);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
    }
    
    static void muenzeAusgeben(int betrag, String einheit) {
//    	System.out.println(betrag + " " + einheit);
    	System.out.printf("    * * *   \n"
        		+ "  *       *\n"           			
        		+ " *    %2s   *  \n"
        		+ " *   %s  * \n"
        		+ "  *       *\n"
        		+ "    * * *   \n", betrag, einheit);
    }
}